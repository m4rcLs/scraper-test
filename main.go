package main

import (
	"fmt"
	"os"
	"regexp"
	"strconv"

	"github.com/gocolly/colly"
)

// Ingredient contains all information about a part of a recipe
type Ingredient struct {
	Name     string
	Amount   int
	Quantity string
}

func (i Ingredient) String() string {
	return fmt.Sprintf("%d %s %s", i.Amount, i.Quantity, i.Name)
}

func main() {
	baseURL := "https://www.hellofresh.de/recipes/"
	recipe := "vegetarisches-moussaka-mit-linsen-5ff573bd1f6d8d5b235f2c5b"
	url := baseURL + recipe

	if len(os.Args) > 1 && os.Args[1] != "" {
		url = os.Args[1]
	}

	c := colly.NewCollector()

	c.OnResponse(func(r *colly.Response) {
		fmt.Println("OnResponse")
		fmt.Println(r.StatusCode)
	})

	c.OnError(func(r *colly.Response, err error) {
		url := r.Request.URL

		fmt.Printf("%s: HTTP %d \n", url, r.StatusCode)
	})

	// div with ingredients
	c.OnHTML("div.fela-_g6xips", func(e *colly.HTMLElement) {
		ingredients := make([]Ingredient, 1)
		ingredient := Ingredient{}

		e.ForEach("p", func(index int, e *colly.HTMLElement) {
			re := regexp.MustCompile(`(\d+) (\S+)`)
			if matched := re.Match([]byte(e.Text)); matched {
				res := re.FindStringSubmatch(e.Text)
				amount := res[1]
				quantity := res[2]
				ingredient.Amount, _ = strconv.Atoi(amount)
				ingredient.Quantity = quantity
			} else {
				ingredient.Name = e.Text
				ingredients = append(ingredients, ingredient)
			}
		})

		for _, ingredient = range ingredients {
			fmt.Println(ingredient)
		}
	})

	c.Visit(url)

}
